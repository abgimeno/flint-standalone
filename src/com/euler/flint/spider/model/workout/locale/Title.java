/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.locale;

import com.euler.flint.spider.model.workout.weight.Movement;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


/**
 *
 * @author root
 */
@Entity
@NamedQueries({
    @NamedQuery(name="LocaleText.getByName", query="SELECT t FROM Title t WHERE t.text =:name")
})
public class Title extends LocaleText{

    
    private Movement movement;

    public Title() {
    }

    public Title(String title) {
        super(title);
    }

    public Title(String text, Language language, Movement movement) {
        super(text, language);
        this.movement= movement;
    }
    @ManyToOne
    public Movement getMovement() {
        return movement;
    }

    public void setMovement(Movement movement) {
        this.movement = movement;
    }
    
  
}
