/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.locale;

import com.euler.flint.spider.model.workout.weight.Movement;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author root
 */

@Entity
public class Description extends LocaleText{
    
    
    private Movement movement;
    
    
    public Description() {
    }

    public Description(String desc) {
        super(desc);
    }    

    public Description(String text, Language language, Movement movement) {
        super(text, language);
        this.movement=movement;
    }
    @ManyToOne
    public Movement getMovement() {
        return movement;
    }

    public void setMovement(Movement movement) {
        this.movement = movement;
    }

}
