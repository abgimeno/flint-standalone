/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.weight;

import com.euler.flint.spider.model.workout.Attribute;
import com.euler.flint.spider.model.workout.locale.LocaleText;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author root
 */
@NamedQueries({
    @NamedQuery(name="MuscleGroup.getMGByName", query="SELECT mg FROM MuscleGroup mg WHERE mg.text =:mgName")
})
@Entity
public class MuscleGroup extends LocaleText implements Serializable {
    
    public static final String QUERY_GETMGBYNAME="MuscleGroup.getMGByName";
    public static final String PARAM_MGNAME="mgName";
     
}
