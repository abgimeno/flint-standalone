/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.weight;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.euler.flint.spider.model.workout.locale.Description;
import com.euler.flint.spider.model.workout.locale.Title;
import java.io.Serializable;
import java.util.List;
import javafx.beans.property.ListProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.collections.FXCollections;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;

/**
 *
 * @author agime
 */
@Entity
@NamedQueries({
    @NamedQuery(name="Movement.getAll", query="SELECT m FROM Movement m")
})
public class Movement implements Serializable {

    private static final long serialVersionUID = 1L;

    private final LongProperty id= new SimpleLongProperty();
    private final ListProperty<Title> title= new SimpleListProperty(FXCollections.observableArrayList());        
    private final ListProperty<Description> description= new SimpleListProperty(FXCollections.observableArrayList()); 
    private final ListProperty<Equipment> equipment= new SimpleListProperty(FXCollections.observableArrayList()); 
    private final ListProperty<MuscleGroup> muscleGroup= new SimpleListProperty(FXCollections.observableArrayList()); 
    private final ListProperty<Difficulty> difficulty= new SimpleListProperty(FXCollections.observableArrayList()); 
    private final ListProperty<MediaUrl> media= new SimpleListProperty(FXCollections.observableArrayList()); 

    
    @Id
    @TableGenerator(name = "movementGen", table = "EJB_ORDER_SEQUENCE_GENERATOR", pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", pkColumnValue = "movementId", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "movementGen")
    public Long getId() {
        return id.get();
    }

    public void setId(Long id) {
        this.id.set(id);
    }
    
    @OneToMany(fetch = FetchType.EAGER)
    public List<MediaUrl> getPhotos() {
        return media.get();
    }

    public void setPhotos(List<MediaUrl> media) {
        this.media.addAll(media);
    }
    @OneToMany(fetch = FetchType.EAGER)
    public List<Title> getTitle() {
        return title.get();
    }

    public void setTitle(List<Title> titles) {
        this.title.addAll(titles);
    }
    @OneToMany(fetch = FetchType.EAGER)
    public List<Description> getDescription() {
        return description;
    }

    public void setDescription(List<Description> description) {
        this.description.addAll(description);
    }

    @ManyToMany
    public List<Equipment> getEquipment() {
        return equipment.get();
    }

    public void setEquipment(List<Equipment> equipment) {
        this.equipment.addAll(equipment);
    }
    @ManyToMany
    public List<MuscleGroup> getMuscleGroup() {
        return muscleGroup.get();
    }

    public void setMuscleGroup(List<MuscleGroup> muscleGroup) {
        this.muscleGroup.addAll(muscleGroup);
    }
    @ManyToMany
    public List<Difficulty> getDifficulty() {
        return difficulty.get();
    }

    public void setDifficulty(List<Difficulty> difficulty) {
        this.difficulty.addAll(difficulty);
    }

    public LongProperty idProperty() {
        return id;
    }

    public ListProperty<Title> titleProperty() {
        return title;
    }

    public ListProperty<Description> descriptionProperty() {
        return description;
    }

    public ListProperty<Equipment> equipmentProperty() {
        return equipment;
    }

    public ListProperty<MuscleGroup> muscleGroupProperty() {
        return muscleGroup;
    }

    public ListProperty<Difficulty> difficultyProperty() {
        return difficulty;
    }

    public ListProperty<MediaUrl> mediaProperty() {
        return media;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movement)) {
            return false;
        }
        Movement other = (Movement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.flint.spider.model.workout.Movement[ id=" + id + " ]";
    }
}
