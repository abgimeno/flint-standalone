/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author root
 */
public class LocalUrl {
    private static final List<String> urls;

    static {
        List<String> aMap = new ArrayList<String>();
        aMap.add("http://localhost:8080/AlternateBentOverDumbbellKickbackExercise.html");
        aMap.add("http://localhost:8080/SupineTricepsExtensiononExerciseBallExercise.html");
        aMap.add("http://localhost:8080/ChestDipExercise.html");
        aMap.add("http://localhost:8080/AlternateLyingDumbbellExtensionExercise.html");        
        
        urls = Collections.unmodifiableList(aMap);
    }

    public static List<String> getUrls() {
        return urls;
    }    
    public static String getEmtpyPage(){
        return "http://localhost:8080/EmptyPage.html";
    }
}
