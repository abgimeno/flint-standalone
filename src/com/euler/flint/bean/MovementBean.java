/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.flint.bean;

import com.euler.flint.spider.model.workout.weight.Movement;
import euler.bugzilla.beans.AbstractBean;
import euler.bugzilla.model.ServerInformation;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author abrahamgimeno
 */
public class MovementBean extends AbstractBean{

    public MovementBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }
    
    public List<Movement> getAllMovements(){
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            List<Movement> moves = (List<Movement>) em.createNamedQuery("Movement.getAll").getResultList();
            em.getTransaction().commit();
            em.close();
            return moves;
    }
 
}
